package main

import (
	"fmt"
	"regexp"
	"syscall/js"
	"time"
)

type provider struct {
	command string
	url     string
	icon    string
	query   string
}

type searchProviders map[string]provider

var commands searchProviders

func getSearchProviders() searchProviders {
	p := make(searchProviders)
	p["google"] = provider{
		url:   "https://www.google.com/search",
		icon:  "fab fa-google",
		query: "q",
	}
	p["amazon"] = provider{
		command: "/a",
		icon:    "fab fa-amazon",
		url:     "https://www.amazon.com/s/",
		query:   "field-keywords",
	}
	p["ddg"] = provider{
		command: "/d",
		icon:    "fab fab-search",
		url:     "https://duckduckgo.com/",
		query:   "q",
	}
	p["reddit"] = provider{
		command: "/r",
		icon:    "fab fab-reddit-alien",
		url:     "https://www.reddit.com/search?sort=relevance&t=all&",
		query:   "q",
	}
	p["wikipedia"] = provider{
		command: "/w",
		icon:    "fab fab-wikipedia-w",
		url:     "http://en.wikipedia.org/wiki/Special:Search/",
		query:   "",
	}
	p["youtube"] = provider{
		command: "/y",
		icon:    "fab fab-youtube",
		url:     "https://www.youtube.com/results",
		query:   "search_query",
	}

	return p
}

func search(this js.Value, args []js.Value) interface{} {
	doc := js.Global().Get("document")
	sInput := doc.Call("getElementById", "search").Get("value")
	q := sInput.String()

	if q == "/?" {
		fmt.Println("help message here")
		dialog := doc.Call("querySelector", "dialog")
		if dialog.Call("showModal", nil).Truthy() {
			fmt.Println("dialog not supported")
		}
		dialog.Call("showModal", nil)
	}

	r := regexp.MustCompile(`^\/.+`)
	if len(q) >= 2 && r.MatchString(q) {
		for k := range commands {
			if q == commands[k].command {
				// Update URL, Icon, and query name
				doc.Call("getElementById", "search").Set("name", commands[k].query)
				doc.Call("getElementById", "search-icon").Set("className", commands[k].icon)
				doc.Call("getElementById", "search-form").Set("action", commands[k].url)
				// Clear out the form...
				doc.Call("getElementById", "search").Set("value", "")
				return nil
			}

			// Couldn't find a matching search provider, fall back to Google
			// Update URL, Icon, and query name
			doc.Call("getElementById", "search").Set("name", commands["google"].query)
			doc.Call("getElementById", "search-icon").Set("className", commands["google"].icon)
			doc.Call("getElementById", "search-form").Set("action", commands["google"].url)
			// Clear out the form...
			doc.Call("getElementById", "search").Set("value", "")
		}
	}

	return nil
}

func main() {
	c := make(chan struct{}, 0)

	commands = getSearchProviders()
	fmt.Println("Go WebAssembly Initialized")

	doc := js.Global().Get("document")
	sCall := js.FuncOf(search)
	doc.Call("addEventListener", "keyup", sCall)
	defer sCall.Release()

	go updateSiteTitle()
	<-c
}

func updateSiteTitle() {
	doc := js.Global().Get("document")

	functions := []func() string{
		displayGreeting(),
		displayTime(),
	}

	for {
		t := doc.Call("getElementById", "title")
		for i := range functions {
			nt := functions[i]()
			t.Set("innerHTML", nt)

			time.Sleep(10 * time.Second)
		}
	}
}

func displayGreeting() func() string {
	return func() string {
		h := time.Now().Hour()
		var msg string
		switch {
		case h >= 0 && h < 12:
			msg = "Morning"
		case h >= 12 && h < 17:
			msg = "Afternoon"
		case h >= 17:
			msg = "Evening"
		}

		return fmt.Sprintf("Good %s, vendion", msg)
	}
}

func displayTime() func() string {
	return func() string {
		t := time.Now().Format("Mon Jan _2 2006 3:04 PM")
		return t
	}
}
